import { LogOut } from "lucide-react";
import { useCallback } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { setLoggedOut } from "@/bootstrap";
import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar";
import { Button } from "@/components/ui/button";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";

export default function Navbar() {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const onHandleLogout = useCallback(() => {
    dispatch(setLoggedOut());
    navigate("/");
  }, [dispatch, navigate]);

  return (
    <nav className="z-10 w-full h-[60px] flex-shrink-0 flex justify-between items-center bg-primary px-6">
      <div
        className="text-2xl text-white font-bold"
        onClick={() => navigate("/")}
      >
        ITEMIFY
      </div>

      <div className="relative">
        <DropdownMenu>
          <DropdownMenuTrigger>
            <Avatar>
              <AvatarImage src="https://github.com/shadcn.png" alt="@shadcn" />
              <AvatarFallback>CN</AvatarFallback>
            </Avatar>
          </DropdownMenuTrigger>
          <DropdownMenuContent className="absolute -top-2 right-0 px-6 py-4 bg-white/70 shadow-lg">
            <DropdownMenuItem>
              <Button
                className="flex items-center gap-4"
                variant="destructive"
                onClick={onHandleLogout}
              >
                <LogOut className="h-4 w-4" />
                <pre>Log out</pre>
              </Button>
            </DropdownMenuItem>
          </DropdownMenuContent>
        </DropdownMenu>
      </div>
    </nav>
  );
}
