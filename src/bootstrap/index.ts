import { createSlice } from "@reduxjs/toolkit";

export interface BootstrapState {
  auth: { isLoggedIn: boolean };
}

const initialState: BootstrapState = {
  auth: { isLoggedIn: false },
};

const bootstrap = createSlice({
  name: "bootstrap",
  initialState,
  reducers: {
    setLoggedIn: (state) => {
      state.auth.isLoggedIn = true;
    },
    setLoggedOut: (state) => {
      state.auth.isLoggedIn = false;
    },
  },
});

export const { setLoggedIn, setLoggedOut } = bootstrap.actions;
export const rootReducer = bootstrap.reducer;
