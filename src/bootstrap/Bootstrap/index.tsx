import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";
import { BootstrapState } from "@/bootstrap/index";

export default function Bootstrap({ children }: { children: React.ReactNode }) {
  const navigate = useNavigate();
  const { pathname } = useLocation();

  const { isLoggedIn } = useSelector((state: BootstrapState) => state.auth);

  const [isLoading, setIsLoading] = useState<boolean>(true);

  useEffect(() => {
    if (!isLoggedIn) {
      navigate("/login");
    }

    if (isLoggedIn && pathname === "/login") {
      navigate("/");
    }

    setIsLoading(false);
  }, [isLoggedIn, navigate, pathname]);

  if (isLoading) return <>Loading ...</>;
  return <>{children}</>;
}
