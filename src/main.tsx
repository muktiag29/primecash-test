import ReactDOM from "react-dom/client";
import { PersistGate } from "redux-persist/integration/react";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Bootstrap from "@/bootstrap/Bootstrap";
import { Toaster } from "@/components/ui/toaster";
import { store, persistor } from "@/configs/store";
import { AddItemPage, AuthPage, DashboardPage } from "@/modules";
import "./index.css";

ReactDOM.createRoot(document.getElementById("root")!).render(
  // <React.StrictMode>
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <BrowserRouter>
        <Bootstrap>
          <Routes>
            <Route path="/" element={<DashboardPage />} />
            <Route path="/login" element={<AuthPage />} />
            <Route path="/add-item" element={<AddItemPage />} />
          </Routes>
          <Toaster />
        </Bootstrap>
      </BrowserRouter>
    </PersistGate>
  </Provider>
  // </React.StrictMode>
);
