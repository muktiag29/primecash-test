import axios from "axios";

const api = axios.create({
  baseURL: "http://localhost:3001",
});

export const getAsyncData = async (path: string) => {
  const response = await api.get(path);
  return response.data;
};

export const postAsyncData = async (path: string, data: unknown) => {
  const response = await api.post(path, data);
  return response.data;
};
