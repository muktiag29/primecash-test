import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  // State awal di sini
};

const mySlice = createSlice({
  name: "myReducer",
  initialState,
  reducers: {
    // Tambahkan action dan reducer di sini
  },
});

export const { actions, reducer } = mySlice;
