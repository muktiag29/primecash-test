import { ColumnDef } from "@tanstack/react-table";
import { TTableItem } from ".";

export const getColumns: (props: {
  activePage: number;
  size: number;
}) => ColumnDef<TTableItem>[] = ({ activePage, size }) => [
  {
    id: "key",
    header: "No.",
    accessorKey: "no",
    cell: ({ row: { index } }) => {
      return <span>{(activePage - 1) * size + index + 1}</span>;
    },
  },
  {
    header: "Name",
    accessorKey: "name",
  },
  {
    header: "Quantity",
    accessorKey: "quantity",
  },
  {
    header: "Stocked Date",
    accessorKey: "stockedDate",
  },
  {
    header: "Condition",
    accessorKey: "condition",
  },
  {
    header: "Category",
    accessorKey: "category",
  },
];
