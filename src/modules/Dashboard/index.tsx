import { ArrowLeft, ArrowRight, PlusSquareIcon } from "lucide-react";
import { useCallback, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Navbar } from "@/components";
import { Button } from "@/components/ui/button";
import {
  ColumnDef,
  flexRender,
  getCoreRowModel,
  useReactTable,
} from "@tanstack/react-table";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table";
import { useToast } from "@/components/ui/use-toast";
import { getAsyncData } from "@/configs/api";
import { getColumns } from "./tableColumn";

type TData = {
  status: "success" | "";
  offset: number;
  size: number;
  totalPage: number;
  totalRecord: number;
  content: TTableItem[];
};

export type TItem = {
  name: string;
  quantity: number;
  stockedDate: string;
  condition: string;
  category: string;
};

export type TTableItem = TItem & { no: number };

export default function DashboardPage() {
  const navigate = useNavigate();
  const { toast } = useToast();

  const [data, setData] = useState<TData>({
    status: "",
    offset: 0,
    size: 0,
    totalPage: 0,
    totalRecord: 0,
    content: [],
  });
  const [pagination, setPagination] = useState<number[]>([]);
  const [activePage, setActivePage] = useState<number>(0);
  const [isFetching, setIsFetching] = useState<boolean>(true);

  const columns: ColumnDef<TTableItem>[] = getColumns({
    activePage,
    size: data?.size,
  });

  const table = useReactTable({
    data: data.content,
    columns,
    getCoreRowModel: getCoreRowModel(),
  });

  const fetchData = useCallback(
    ({ size, page }: { size?: number; page?: number }) => {
      const _size = size || 3;
      const _offset = (size || 0) * (page ? page - 1 : 0) || 0;

      getAsyncData(`/items?size=${_size}&&offset=${_offset}`)
        .then((_response) => {
          if (_response?.status !== "success") {
            toast({
              title: "Error",
              description: "Failed to Get Data",
            });
            return;
          }

          setData(_response as TData);
          setActivePage(page || 1);

          if (!page) {
            setPagination(
              Array(_response?.totalPage < 4 ? _response?.totalPage : 3)
                .fill({})
                .map((_, _index) => _index + 1)
            );
          }
        })
        .catch(() => {
          toast({
            title: "Error",
            description: "Failed to Get Data",
          });
        })
        .finally(() => {
          setIsFetching(false);
        });
    },
    [toast]
  );

  const onHandlePrevPage = useCallback(() => {
    setPagination((_page) => _page?.map((e) => e - 1));
  }, []);

  const onHandleNextPage = useCallback(() => {
    setPagination((_page) => _page?.map((e) => e + 1));
  }, []);

  const onHandleSelectPage = useCallback(
    (_page: number) => {
      fetchData({ size: data?.size, page: _page });
    },
    [data?.size, fetchData]
  );

  useEffect(() => {
    fetchData({});
  }, [fetchData]);

  return (
    <>
      <main className="w-screen h-[100svh] flex flex-col">
        <Navbar />

        <section className="w-full h-full p-6">
          <Table>
            <TableHeader>
              {table.getHeaderGroups().map((headerGroup) => (
                <TableRow key={headerGroup.id}>
                  {headerGroup.headers.map((header) => {
                    return (
                      <TableHead key={header.id} className="whitespace-nowrap">
                        {header.isPlaceholder
                          ? null
                          : flexRender(
                              header.column.columnDef.header,
                              header.getContext()
                            )}
                      </TableHead>
                    );
                  })}
                </TableRow>
              ))}
            </TableHeader>

            <TableBody>
              {table.getRowModel().rows?.length ? (
                table.getRowModel().rows.map((row) => (
                  <TableRow
                    key={row.id}
                    data-state={row.getIsSelected() && "selected"}
                  >
                    {row.getVisibleCells().map((cell) => (
                      <TableCell key={cell.id}>
                        {flexRender(
                          cell.column.columnDef.cell,
                          cell.getContext()
                        )}
                      </TableCell>
                    ))}
                  </TableRow>
                ))
              ) : (
                <TableRow>
                  <TableCell
                    colSpan={columns?.length}
                    className="h-24 text-center"
                  >
                    {isFetching ? "Loading ..." : "No Data"}
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>

          {!!data?.totalPage && (
            <div className="flex flex-col md:flex-row justify-between items-start md:items-center gap-3 mt-4">
              <pre className="text-sm">
                Show {data.size} from {data.totalRecord}
              </pre>

              <div className="flex gap-2 font-semibold text-white">
                <Button
                  className="w-6 aspect-square bg-primary grid place-content-center border"
                  onClick={onHandlePrevPage}
                  disabled={pagination[0] === 1}
                >
                  <ArrowLeft className="w-4 h-4" />
                </Button>

                {pagination.map((_page) => (
                  <Button
                    key={_page}
                    className="w-6 aspect-square bg-primary grid place-content-center border"
                    variant="destructive"
                    onClick={() => onHandleSelectPage(_page)}
                  >
                    {_page}
                  </Button>
                ))}

                <Button
                  className="w-6 aspect-square bg-primary grid place-content-center border"
                  onClick={onHandleNextPage}
                  disabled={
                    pagination[pagination.length - 1] === data?.totalPage
                  }
                >
                  <ArrowRight className="w-4 h-4 font-white" />
                </Button>
              </div>
            </div>
          )}
        </section>
      </main>

      <div className="fixed bottom-6 right-6">
        <Button className="flex gap-4" onClick={() => navigate("/add-item")}>
          <PlusSquareIcon className="w-6 h-6" />
          <div>Add Item</div>
        </Button>
      </div>
    </>
  );
}
