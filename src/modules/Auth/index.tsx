import { useCallback } from "react";
import { useForm, SubmitHandler, FieldValues } from "react-hook-form";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { setLoggedIn } from "@/bootstrap";
import { Button } from "@/components/ui/button";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { useToast } from "@/components/ui/use-toast";
import { postAsyncData } from "@/configs/api";

export default function AuthPage() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const form = useForm();
  const { toast } = useToast();

  const onHandleLogin: SubmitHandler<FieldValues> = useCallback(
    (_data) => {
      postAsyncData("/login", _data)
        .then((_response) => {
          if (_response?.status === "success") {
            dispatch(setLoggedIn());
            navigate("/");
          } else {
            toast({
              title: "Error",
              description: "Failed to Login",
            });
          }
        })
        .catch(() => {
          toast({
            title: "Error",
            description: "Failed to Login",
          });
        });
    },
    [dispatch, navigate, toast]
  );

  return (
    <main className="w-screen h-[100svh] flex justify-center items-center">
      <Card className="w-full max-w-md mx-6">
        <CardHeader className="text-center">
          <CardTitle className="text-primary">ITEMIFY</CardTitle>
          <CardDescription>Manage your storage easily.</CardDescription>
        </CardHeader>

        <Form {...form}>
          <form onSubmit={form.handleSubmit(onHandleLogin)}>
            <CardContent>
              <div className="grid w-full items-center gap-4">
                <FormField
                  control={form.control}
                  name="username"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Username</FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          placeholder="Username"
                          value={field.value || ""}
                          required
                        />
                      </FormControl>
                    </FormItem>
                  )}
                />

                <FormField
                  control={form.control}
                  name="password"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Username</FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          type="password"
                          placeholder="Password"
                          value={field.value || ""}
                          required
                        />
                      </FormControl>
                    </FormItem>
                  )}
                />
              </div>
            </CardContent>

            <CardFooter className="flex justify-end">
              <Button variant="default">Login</Button>
            </CardFooter>
          </form>
        </Form>
      </Card>
    </main>
  );
}
