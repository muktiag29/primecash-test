import { Check } from "lucide-react";
import { useNavigate } from "react-router-dom";
import { Button } from "@/components/ui/button";
import { CardContent, CardFooter } from "@/components/ui/card";
import { Itemlabels } from "@/modules/AddItem/StepPreview";
import { TItem } from "@/modules/Dashboard";

type TProps = {
  data: TItem | null;
};

export default function AddItemPage({ data }: TProps) {
  const navigate = useNavigate();

  return (
    <>
      <CardContent className="space-y-3">
        {Itemlabels.map((_item, _index) => {
          return (
            <div key={_index}>
              <div className="text-lg font-semibold">{_item?.label}</div>
              <small className="text-sm font-medium leading-none">
                {data ? data?.[_item?.key]?.toString() || "-" : "-"}
              </small>
            </div>
          );
        })}
      </CardContent>

      <CardFooter className="flex gap-4 justify-end">
        <Button
          type="submit"
          className="flex gap-3"
          onClick={() => navigate("/")}
        >
          <Check className="w-4 h-4" />
          <span>Done</span>
        </Button>
      </CardFooter>
    </>
  );
}
