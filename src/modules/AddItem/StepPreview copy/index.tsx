import { Save } from "lucide-react";
import { useCallback } from "react";
import { Button } from "@/components/ui/button";
import { CardContent, CardFooter } from "@/components/ui/card";
import { postAsyncData } from "@/configs/api";
import { TItem } from "@/modules/Dashboard";

type TProps = {
  data: TItem | null;
};
type TItemKeys = keyof TItem;

const labels: { label: string; key: TItemKeys }[] = [
  { label: "Name", key: "name" },
  { label: "Quantity", key: "quantity" },
  { label: "Stocked Date", key: "stockedDate" },
  { label: "Condition", key: "condition" },
  { label: "Category", key: "category" },
];

export default function AddItemPage({ data }: TProps) {
  const onHandleSubmit = useCallback(() => {
    postAsyncData("/add-items", data)
      .then((_response) => {
        // BERHASIL
        console.log({ _response });
      })
      .catch((_error) => console.log(_error));
  }, [data]);

  return (
    <>
      <CardContent className="space-y-3">
        {labels.map((_item, _index) => {
          return (
            <div key={_index}>
              <div className="text-lg font-semibold">{_item?.label}</div>
              <small className="text-sm font-medium leading-none">
                {data ? data?.[_item?.key]?.toString() || "-" : "-"}
              </small>
            </div>
          );
        })}
      </CardContent>

      <CardFooter className="flex gap-4 justify-end">
        <Button type="submit" className="flex gap-3" onClick={onHandleSubmit}>
          <Save className="w-4 h-4" />
          <span>Save</span>
        </Button>
      </CardFooter>
    </>
  );
}
