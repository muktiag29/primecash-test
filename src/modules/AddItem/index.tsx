import { useCallback, useMemo, useState } from "react";
import { FieldValues, SubmitHandler } from "react-hook-form";
import { Navbar } from "@/components";
import { postAsyncData } from "@/configs/api";
import { Card, CardHeader, CardTitle } from "@/components/ui/card";
import { useToast } from "@/components/ui/use-toast";
import { TItem } from "@/modules/Dashboard";
import StepForm from "./StepForm";
import StepPreview from "./StepPreview";
import StepView from "./StepView";

export default function AddItemPage() {
  const { toast } = useToast();

  const [stepIndex, setStepIndex] = useState<number>(0);
  const [data, setData] = useState<TItem | null>(null);

  const onHandlePreview: SubmitHandler<FieldValues> = useCallback((_data) => {
    setData(_data as TItem);
    setStepIndex((_prev) => _prev + 1);
  }, []);

  const onHandleSubmit = useCallback(() => {
    postAsyncData("/add-items", data)
      .then((_response) => {
        if (_response?.status === "success") {
          setStepIndex((_prev) => _prev + 1);
        } else {
          toast({
            title: "Error",
            description: "Failed to Add Data",
          });
        }
      })
      .catch(() => {
        toast({
          title: "Error",
          description: "Failed to Add Data",
        });
      });
  }, [data, toast]);

  const RenderTitle = useMemo(() => {
    switch (stepIndex) {
      case 0:
        return "Add Item";
      case 1:
        return "Preview Item";
      case 2:
        return "View Item";
      default:
        return null;
    }
  }, [stepIndex]);

  const RenderContent = useMemo(() => {
    switch (stepIndex) {
      case 0:
        return <StepForm onHandleSubmit={onHandlePreview} />;
      case 1:
        return <StepPreview data={data} onHandleSubmit={onHandleSubmit} />;
      case 2:
        return <StepView data={data} />;
      default:
        return null;
    }
  }, [data, onHandlePreview, onHandleSubmit, stepIndex]);

  return (
    <main className="w-screen h-[100svh] flex flex-col">
      <Navbar />

      <div className="w-full h-full p-6">
        <Card>
          <CardHeader>
            <CardTitle className="text-primary">{RenderTitle}</CardTitle>
          </CardHeader>
          {RenderContent}
        </Card>
      </div>
    </main>
  );
}
