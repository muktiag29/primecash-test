import { Save } from "lucide-react";
import { Button } from "@/components/ui/button";
import { CardContent, CardFooter } from "@/components/ui/card";
import { TItem } from "@/modules/Dashboard";

type TProps = {
  data: TItem | null;
  onHandleSubmit: () => void;
};
type TItemKeys = keyof TItem;

export const Itemlabels = [
  { label: "Name", key: "name" },
  { label: "Quantity", key: "quantity" },
  { label: "Stocked Date", key: "stockedDate" },
  { label: "Condition", key: "condition" },
  { label: "Category", key: "category" },
] satisfies { label: string; key: TItemKeys }[];

export default function AddItemPage({ data, onHandleSubmit }: TProps) {
  return (
    <>
      <CardContent className="space-y-3">
        {Itemlabels.map((_item, _index) => {
          return (
            <div key={_index}>
              <div className="text-lg font-semibold">{_item?.label}</div>
              <small className="text-sm font-medium leading-none">
                {data ? data?.[_item?.key]?.toString() || "-" : "-"}
              </small>
            </div>
          );
        })}
      </CardContent>

      <CardFooter className="flex gap-4 justify-end">
        <Button type="submit" className="flex gap-3" onClick={onHandleSubmit}>
          <Save className="w-4 h-4" />
          <span>Save</span>
        </Button>
      </CardFooter>
    </>
  );
}
