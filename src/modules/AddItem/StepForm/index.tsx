import { View } from "lucide-react";
import { useEffect, useState } from "react";
import { FieldValues, SubmitHandler, useForm } from "react-hook-form";
import { FormDatePicker, FormDropDown } from "@/components";
import { IDataDropDown } from "@/components/FormDropDown";
import { Button } from "@/components/ui/button";
import { CardContent, CardFooter } from "@/components/ui/card";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { getAsyncData } from "@/configs/api";

type TProps = {
  onHandleSubmit: SubmitHandler<FieldValues>;
};

const dataCondition: IDataDropDown[] = [
  {
    value: "NEW",
    label: "NEW",
  },
  {
    value: "USED",
    label: "USED",
  },
  {
    value: "DAMAGED",
    label: "DAMAGED",
  },
];

export default function AddItemPage({ onHandleSubmit }: TProps) {
  const form = useForm();

  const [dataCategory, setDataCategory] = useState<IDataDropDown[]>([]);

  useEffect(() => {
    getAsyncData("/items-categories").then((_response) => {
      setDataCategory(
        _response?.content?.map((e: { category: string }) => ({
          label: e?.category,
          value: e?.category,
        }))
      );
    });
  }, []);

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onHandleSubmit)}>
        <CardContent>
          <div className="grid grid-cols-1 md:grid-cols-2 w-full items-center gap-4">
            <FormField
              control={form.control}
              name="name"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Item Name</FormLabel>
                  <FormControl>
                    <Input
                      {...field}
                      placeholder="Item Name"
                      value={field.value || ""}
                      required
                    />
                  </FormControl>
                </FormItem>
              )}
            />

            <FormField
              control={form.control}
              name="quantity"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Quantity</FormLabel>
                  <FormControl>
                    <Input
                      {...field}
                      type="number"
                      min={1}
                      placeholder="Quantity"
                      value={field.value || ""}
                      required
                    />
                  </FormControl>
                </FormItem>
              )}
            />

            <FormDatePicker
              form={form}
              name="stockedDate"
              label="Stocked Date"
              placeholder="Set Stocked Date"
            />

            <FormDropDown
              form={form}
              name="condition"
              label="Condition"
              placeholder="Select Condition"
              data={dataCondition}
            />

            <FormDropDown
              form={form}
              name="category"
              label="Category"
              placeholder="Select Category"
              data={dataCategory}
            />
          </div>
        </CardContent>

        <CardFooter className="flex gap-4 justify-end">
          <Button type="submit" className="flex gap-3">
            <View className="w-4 h-4" />
            <span>Preview</span>
          </Button>
        </CardFooter>
      </form>
    </Form>
  );
}
